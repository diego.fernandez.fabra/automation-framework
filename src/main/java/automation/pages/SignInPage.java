package automation.pages;

import automation.drivers.DriverSingleton;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import automation.utils.Utils;

public class SignInPage {
    private WebDriver driver;

    public SignInPage(){
        this.driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this); //inicializar los elementos y permite usar los findby
    }

    @FindBy(id = "email")
    private WebElement emailField; //lineas 17 y 18 son el equivalente a driver.FindElementBy

    @FindBy(id = "passwd")
    private WebElement password;

    @FindBy(id = "SubmitLogin")
    private WebElement signInButton;

    @FindBy(id = "email_create")
    private WebElement emailSignUpField;

    @FindBy(id = "SubmitCreate")
    private WebElement signUpButton;

    //metodo de login
    public void logIn(String email, String passwd){
        emailField.sendKeys(email);
        password.sendKeys(Utils.decode64(passwd));
        signInButton.click();

    }
    //metodo de signup
    public void signUp(String email){
        emailSignUpField.sendKeys(email);
        signUpButton.click();
    }


}

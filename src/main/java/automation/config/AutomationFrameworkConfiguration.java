package automation.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("automation")
public class AutomationFrameworkConfiguration {
    public AutomationFrameworkConfiguration(){}  //class for searching the configuration of the ConfigurationProperties.java file


}


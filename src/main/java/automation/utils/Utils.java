package automation.utils;

import automation.drivers.DriverSingleton;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.Random;

public class Utils {
    public static int testCount = 0;
    public static String decode64(String encodedStr){  //decode method for passwords or others that are in Base64 encoded
        Base64.Decoder decoder = Base64.getDecoder();
        return new String(decoder.decode(encodedStr.getBytes()));
    }
    public static boolean takeScreenshot(){
        //converting the driver into the TakeScreenshot interface. The screenshot will be locate in a file in a location of the disk
        File file = ((TakesScreenshot) DriverSingleton.getDriver()).getScreenshotAs(OutputType.FILE);
        try {
            FileCopyUtils.copy(file, new File(Constants.SCREENSHOTS_FOLDER + generateRandomString(Constants.SCREENSHOT_NAME_LENGTH) + Constants.SCREENSHOT_EXTENSION));
            return true;
        }catch (IOException e){
            return false;
        }
    }
    //Method to create random names for the screenshots (if this is not implemented, all the screenshots will have the same name)
    private static String generateRandomString(int length){
        String seedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder sb = new StringBuilder(); //creator of the strings
        int i = 0;
        Random random = new Random();
        //printing a random character each time until the number of characters is reached
        while(i < length){
            sb.append(seedChars.charAt(random.nextInt(seedChars.length())));
            i++;
        }
        return sb.toString();
    }
}
